package api.univ_playground;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnivPlaygroundApplication {

    public static void main(String[] args) {
        SpringApplication.run(UnivPlaygroundApplication.class, args);
    }

}
